FROM golang:1.10.7
RUN mkdir -p /go/src/dockerDemoGo
WORKDIR /go/src/dockerDemoGo
COPY . /go/src/dockerDemoGo
CMD ["go-wrapper", "run"]
ONBUILD COPY . /go/src/dockerDemoGo
ONBUILD RUN go-wrapper download
ONBUILD RUN go-wrapper install
RUN go get github.com/astaxie/beego
RUN go get -d -v
RUN go install -v
