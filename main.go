package main

import (
	//_ "dockerDemoGo/routers"
	"github.com/astaxie/beego"
	"dockerDemoGo/controllers"
)

func main() {
	beego.Router("/", &controllers.MainController{})
	beego.Run()
}

